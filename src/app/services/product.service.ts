import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { IProduct } from './../interfaces/IProduct';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private JSON_URL = 'http://localhost:3000/products';
  constructor(private http: HttpClient) { }

  fetchProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(this.JSON_URL).pipe(delay(500));
  }

  addProduct(product: IProduct): Observable<IProduct> {
    return this.http.post<IProduct>(this.JSON_URL, product);
  }

  editProduct(product: IProduct): Observable<IProduct> {
    return this.http.put<IProduct>(this.JSON_URL + '/' + product.id, product);
  }

  removeProduct(id: number): Observable<void> {
    return this.http.delete<void>(this.JSON_URL + '/' + id);
  }
}
