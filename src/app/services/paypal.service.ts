import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaypalService {

  intervalId: any;

  constructor() { }

  private loadExternalScript(scriptUrl: string) {
    return new Promise((resolve, reject) => {
      const scriptElement = document.createElement('script');
      scriptElement.src = scriptUrl;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    });
  }

  getButtons(): Promise<any> {
    clearInterval(this.intervalId);
    return this.loadExternalScript(
      'https://www.paypal.com/sdk/js?client-id=ASb9533qW2VAYXCQBab6rgKaCgsuVsjf7F2gz_ESfrMs0WJ8ceoEKdc40Clk_KJD8k7jWHb41d2fIGeY');
  }
}
