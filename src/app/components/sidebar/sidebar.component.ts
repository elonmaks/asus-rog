import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

  @Output() closeControl = new EventEmitter<boolean>();
  @Output() showAddForm = new EventEmitter<boolean>();
  @Output() showRemoveForm = new EventEmitter<boolean>();
  @Output() showEditForm = new EventEmitter<boolean>();

  hideButtons = false;

  constructor() { }

  onReturn() {
    this.hideButtons = false;
    this.closeControl.emit();
  }

  onAddProduct() {
    this.showAddForm.emit();
    this.hideButtons = !this.hideButtons;
  }

  onEditProduct() {
    this.showEditForm.emit();
    this.hideButtons = !this.hideButtons;
  }

  onRemoveProduct() {
    this.showRemoveForm.emit();
    this.hideButtons = !this.hideButtons;
  }
}
