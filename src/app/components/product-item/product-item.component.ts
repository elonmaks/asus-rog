import { IProduct } from '../../interfaces/IProduct';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})

export class ProductItemComponent {

  @Input() product: IProduct;
  @Input() addRemoveForm: boolean;
  @Input() addEditForm: boolean;
  @Output() removeProduct = new EventEmitter<number>();
  @Output() editProduct = new EventEmitter<IProduct>();

  constructor(private router: Router) { }

  onAction() {
    if (this.addEditForm) {
      this.editProduct.emit(this.product);
    }
    if (this.addRemoveForm) {
      this.removeProduct.emit(this.product.id);
    }
  }

  moveToCart() {
    this.router.navigate(['/cart'], { state: { data: { product: this.product } } });
  }
}
