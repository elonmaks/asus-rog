import { ProductService } from './../../services/product.service';
import { IProduct } from './../../interfaces/IProduct';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {

  products: IProduct[];
  editingProduct: IProduct = null;
  loading = false;
  displayItems = true;
  addRemoveForm = false;
  addEditForm = false;

  constructor(private productsService: ProductService) { }

  ngOnInit(): void {
    this.fetchProducts();
  }

  fetchProducts() {
    this.loading = true;
    this.productsService.fetchProducts()
      .subscribe(productsData => {
        this.products = productsData;
        this.loading = false;
      });
  }

  showAddForm() {
    this.displayItems = false;
  }

  showEditForm() {
    this.addEditForm = true;
  }

  showRemoveForm() {
    this.addRemoveForm = true;
  }

  onCloseControl() {
    this.displayItems = true;
    this.addRemoveForm = false;
    this.addEditForm = false;
  }

  editProduct(product: IProduct) {
    this.editingProduct = product;
    this.displayItems = false;
  }
}
