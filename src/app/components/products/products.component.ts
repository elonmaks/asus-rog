import { ProductService } from '../../services/product.service';
import { IProduct } from '../../interfaces/IProduct';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {

  @Input() addRemoveForm: boolean;
  @Input() addEditForm: boolean;
  @Input() products: IProduct[];
  @Output() editProduct = new EventEmitter<IProduct>();
  error: '';

  constructor(private productsService: ProductService) { }

  onEditProduct(product: IProduct) {
    this.editProduct.emit(product);
  }

  removeProduct(id: number) {
    if (this.addRemoveForm) {
      this.productsService.removeProduct(id)
        .subscribe(() => {
          this.products = this.products.filter(product => product.id !== id);
        }, error => this.error = error.message);
    }
  }
}
