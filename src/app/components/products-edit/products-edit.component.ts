import { IProduct } from './../../interfaces/IProduct';
import { ProductService } from '../../services/product.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, Form, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-products-edit',
  templateUrl: './products-edit.component.html',
  styleUrls: ['./products-edit.component.scss']
})
export class ProductsEditComponent implements OnInit {

  @Input() products: IProduct[];
  @Input() productOnEdit: IProduct;
  productWasAdded = false;
  form: FormGroup;
  error = '';

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    if (this.productOnEdit === null) {
      this.form = new FormGroup({
        title: new FormControl('', [
          Validators.required,
          Validators.minLength(5)
        ]),
        description: new FormArray([], [
          Validators.required
        ]),
        screenSize: new FormControl(null, [
          Validators.required
        ]),
        imagePath: new FormControl('', [
          Validators.required,
          Validators.minLength(8)
        ]),
        price: new FormControl(null, [
          Validators.required
        ]),
      });
    } else {
      this.form = new FormGroup({
        title: new FormControl(this.productOnEdit.title, [
          Validators.required,
          Validators.minLength(3)
        ]),
        description: new FormArray([], [
          Validators.required
        ]),
        screenSize: new FormControl(this.productOnEdit.screenSize, [
          Validators.required
        ]),
        imagePath: new FormControl(this.productOnEdit.imagePath, [
          Validators.required,
          Validators.minLength(8)
        ]),
        price: new FormControl(this.productOnEdit.price, [
          Validators.required
        ]),
      });
    }
  }

  get title() {
    return this.form.get('title');
  }

  get description() {
    return this.form.get('description') as FormArray;
  }

  get screenSize() {
    return this.form.get('screenSize');
  }

  get imagePath() {
    return this.form.get('imagePath');
  }

  get price() {
    return this.form.get('price');
  }

  addDescription() {
    const control = new FormControl('', Validators.required);
    (this.form.get('description') as FormArray).push(control);
  }

  addProduct() {
    if (this.form.valid) {
      const formData = { ...this.form.value };
      const prod = {
        id: Math.floor(Math.random() * 100),
        title: formData.title,
        description: formData.description,
        screenSize: formData.screenSize,
        imagePath: formData.imagePath,
        price: formData.price
      };
      this.productService.addProduct(prod)
        .subscribe(
          () => this.products.unshift(prod),
          error => this.error = error.message);
      this.productWasAdded = true;
      this.form.reset();
    }
  }

  onEditProduct() {
    if (this.form.valid) {
      const formData = { ...this.form.value };
      const prod = {
        id: this.productOnEdit.id,
        title: formData.title,
        description: formData.description,
        screenSize: formData.screenSize,
        imagePath: formData.imagePath,
        price: formData.price
      };
      this.productService.editProduct(prod)
        .subscribe(() => { },
          error => this.error = error.message);
      this.productWasAdded = true;
      this.form.reset();
    }
  }
}
