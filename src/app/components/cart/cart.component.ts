import { Router } from '@angular/router';
import { ProductService } from './../../services/product.service';
import { PaypalService } from '../../services/paypal.service';
import { IProduct } from './../../interfaces/IProduct';
import { Component, OnInit } from '@angular/core';

declare const paypal: any;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  product: IProduct;
  payerInfo: any;
  orderId: number;
  purchase = false;
  shippingData: any;

  constructor(
    private paypalService: PaypalService,
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (history.state.data !== undefined) {
      this.product = history.state.data.product;
      this.paypalService.getButtons()
        .then(() => {
          paypal.Buttons({
            createOrder: (data, actions) => {
              return actions.order.create({
                purchase_units: [{
                  amount: {
                    value: this.product.price.toFixed(2)
                  }
                }]
              });
            },
            onApprove: (data, actions) => {
              return actions.order.capture().then(details => {
                this.payerInfo = details.payer;
                console.log(details, data);
                this.shippingData = details.purchase_units[0].shipping.address;
                this.orderId = data.orderID;
                this.purchase = true;
              });
            }
          })
            .render('.paypal-button-container');
        });
    }
  }

  onReturn() {
    const prod = {
      ...this.product,
      boughtCounter: this.product.boughtCounter + 1
    };
    this.productService.editProduct(prod).subscribe();
    this.router.navigate(['/']);
  }
}






