export interface IProduct {
    id?: number;
    title: string;
    description: string[];
    screenSize: number;
    imagePath: string;
    boughtCounter?: number;
    price?: number;
}
